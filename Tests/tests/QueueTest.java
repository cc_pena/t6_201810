package tests;

import org.junit.*;
import junit.framework.TestCase;

import model.data_structures.Queue;
import model.vo.Taxi;


/**
 * Clase usada para verificar que la estructura Stack (pila) est� correctamente implementada.
 */
public class QueueTest extends TestCase
{

	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

	private Queue<Taxi> cola;
	
	public void setUp()
	{
		cola = new Queue<Taxi>(); 
	}
	
    // -----------------------------------------------------------------
    // M�todos de prueba
    // -----------------------------------------------------------------

	/**
	 * Prueba 1: Verificar el m�todo push
	 * <b>M�todos a probar:</b> <br>
	 * push
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila est� vac�a
	 * 2) La pila tiene elementos
	 */
	public void testEnqueue() 
	{
		//Caso 1
			//Verifica que la pila est� vac�a
		assertEquals(0, cola.size()); 
			//Crea un taxi para agregarlo a la pila
		Taxi prueba1 = new Taxi("taxiID1");
			//Agrega el taxi a la pila
		cola.enqueue(prueba1);
			//Verifica que se haya agregado a la pila
//		assertNotNull("Deberia haber agregado un objeto a la pila", cola.get(prueba1));
		
		//Caso 2
			//ya se ha agregado un elemento a la pila, se verifica que lo tenga
		assertEquals(1, cola.size());
			//Crea un nuevo taxi para agregarlo a la pila
		Taxi prueba2 = new Taxi("taxiID2");
			//Agrega el taxi a la pila
		cola.enqueue(prueba2);
			//Verifica que se haya agregado a la pila con el tama�o y luego obteniendo el elemento
//		assertNotNull("Deberia haber agregado un objeto a la pila", cola.get(prueba2));
		assertEquals(2, cola.size());
	}
	
	/**
	 * Prueba 1: Verificar el m�todo pop
	 * <b>M�todos a probar:</b> <br>
	 * pop
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila est� vac�a
	 * 2) La pila tiene solo el elemento que se quiere eliminar
	 * 3) La pila tiene mas de un elemento
	 */
	public void testDequeue() 
	{
		//Caso 1
			//Verifica que la pila est� vac�a
		assertEquals(0, cola.size()); 
			//No se pudo quitar ningun elemento de la pila, el elemento quitado es null, debe retornar eso
		assertEquals(null, cola.dequeue());
		
		//Caso 2
			//Crea un taxi para agregarlo a la pila
		Taxi prueba1 = new Taxi("taxiID1");
		cola.enqueue(prueba1);
			//Se verifica que se agrego un elemento
		assertEquals(1, cola.size());
			//Se quita el elemento prueba1 de la pila, debe retornar ese elemento
		assertEquals(prueba1, cola.dequeue());
			//Se verifica que no hay elementos en la pila
		assertEquals(0, cola.size());
		
		//Caso 3
		Taxi prueba2 = new Taxi("taxiID2");
			//Prueba 2 is on top of the stack
		cola.enqueue(prueba1);cola.enqueue(prueba2);
		System.out.println(prueba1);
		System.out.println(prueba2);
			//El elemento eliminado debe ser el que est� on top of the stack
		assertEquals(prueba1, cola.dequeue());
		
	}
	
	/**
	 * Prueba 1: Verificar el m�todo get
	 * <b>M�todos a probar:</b> <br>
	 * get
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila tiene el elemnto buscado
	 * 2) La pila no tiene el elemnto buscado
	 */
	public void testGet()
	{
		//Caso 1:
			//Se crean dos elementos y se agregan a la pila
		Taxi prueba1 = new Taxi("taxiID1");
		Taxi prueba2 = new Taxi("taxiID2");

			//Prueba 2 is on top of the stack
		cola.enqueue(prueba1);cola.enqueue(prueba2);
		
			//Se obtiene el elemento
//		assertTrue(cola.get(prueba1));
//		assertTrue(cola.get(prueba2));
		
		//Caso 2:
			//Se crea un nuevo elemento que se va a buscar en la pila pero no se agrega
		Taxi prueba3 = new Taxi("taxiID3");
			//Se busca el elemento en la pila
//		assertFalse(cola.get(prueba3));
		
	}
	
	/**
	 * Prueba 1: Verificar el m�todo isEmpty
	 * <b>M�todos a probar:</b> <br>
	 * isEmpty
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila est� vac�a
	 * 2) La pila no est� vac�a
	 */
	public void testIsEmpty()
	{
		//Caso 1:
		assertTrue(cola.isEmpty());
		
		//Caso 2:
			//Se crea un taxi y se agrega a la pila
		Taxi prueba1 = new Taxi("taxiID1");
		cola.enqueue(prueba1);
			//Se verifica que la pila no est� vac�a
		assertFalse(cola.isEmpty());
	}
	
	
}
