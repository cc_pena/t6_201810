package tests;

import org.junit.*;
import junit.framework.TestCase;

import model.data_structures.Stack;
import model.vo.Taxi;
import model.data_structures.Node;


/**
 * Clase usada para verificar que la estructura Stack (pila) est� correctamente implementada.
 */
public class StackTest extends TestCase
{

	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

	private Stack<Taxi> pila;
	
	public void setUp()
	{
		pila = new Stack<Taxi>(); 
	}
	
    // -----------------------------------------------------------------
    // M�todos de prueba
    // -----------------------------------------------------------------

	/**
	 * Prueba 1: Verificar el m�todo push
	 * <b>M�todos a probar:</b> <br>
	 * push
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila est� vac�a
	 * 2) La pila tiene elementos
	 */
	public void testPush() 
	{
		//Caso 1
			//Verifica que la pila est� vac�a
		assertEquals(0, pila.size); 
			//Crea un taxi para agregarlo a la pila

		Taxi prueba1 = new Taxi("taxiID1");
			//Agrega el taxi a la pila
		pila.push(prueba1);
			//Verifica que se haya agregado a la pila
		assertNotNull("Deberia haber agregado un objeto a la pila", pila.get(prueba1));
		
		//Caso 2
			//ya se ha agregado un elemento a la pila, se verifica que lo tenga
		assertEquals(1, pila.size);
			//Crea un nuevo taxi para agregarlo a la pila
		Taxi prueba2 = new Taxi("taxiID2");
			//Agrega el taxi a la pila
		pila.push(prueba2);
			//Verifica que se haya agregado a la pila con el tama�o y luego obteniendo el elemento
		assertNotNull("Deberia haber agregado un objeto a la pila", pila.get(prueba2));
		assertEquals(2, pila.size);
	}
	
	/**
	 * Prueba 1: Verificar el m�todo pop
	 * <b>M�todos a probar:</b> <br>
	 * pop
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila est� vac�a
	 * 2) La pila tiene solo el elemento que se quiere eliminar
	 * 3) La pila tiene mas de un elemento
	 */
	public void testPop() 
	{
		//Caso 1
			//Verifica que la pila est� vac�a
		assertEquals(0, pila.size); 
			//No se pudo quitar ningun elemento de la pila, el elemento quitado es null, debe retornar eso
		assertEquals(null, pila.pop());
		
		//Caso 2
			//Crea un taxi para agregarlo a la pila
		Taxi prueba1 = new Taxi("taxiID1");
		pila.push(prueba1);
			//Se verifica que se agrego un elemento
		assertEquals(1, pila.size);
			//Se quita el elemento prueba1 de la pila, debe retornar ese elemento
		assertEquals(prueba1, pila.pop());
			//Se verifica que no hay elementos en la pila
		assertEquals(0, pila.size);
		
		//Caso 3
		Taxi prueba2 = new Taxi("taxiID2");
			//Prueba 2 is on top of the stack
		pila.push(prueba1);pila.push(prueba2);
			//El elemento eliminado debe ser el que est� on top of the stack
		assertEquals(prueba2, pila.pop());
		
	}
	
	/**
	 * Prueba 1: Verificar el m�todo get
	 * <b>M�todos a probar:</b> <br>
	 * get
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila tiene el elemnto buscado
	 * 2) La pila no tiene el elemnto buscado
	 */
	public void testGet()
	{
		//Caso 1:
			//Se crean dos elementos y se agregan a la pila
		Taxi prueba1 = new Taxi("taxiID1");
		Taxi prueba2 = new Taxi("taxiID2");
			//Prueba 2 is on top of the stack
		pila.push(prueba1);pila.push(prueba2);
		
			//Se obtiene el elemento
		assertTrue(pila.get(prueba1));
		assertTrue(pila.get(prueba2));
		
		//Caso 2:
			//Se crea un nuevo elemento que se va a buscar en la pila pero no se agrega
		Taxi prueba3 = new Taxi("taxiID3");
			//Se busca el elemento en la pila
		assertFalse(pila.get(prueba3));
		
	}
	
	/**
	 * Prueba 1: Verificar el m�todo isEmpty
	 * <b>M�todos a probar:</b> <br>
	 * isEmpty
	 * <b> Casos de prueba: </b><br>
	 * 1) La pila est� vac�a
	 * 2) La pila no est� vac�a
	 */
	public void testIsEmpty()
	{
		//Caso 1:
		assertTrue(pila.isEmpty());
		
		//Caso 2:
			//Se crea un taxi y se agrega a la pila
		Taxi prueba1 = new Taxi("taxiID1");
		pila.push(prueba1);
			//Se verifica que la pila no est� vac�a
		assertFalse(pila.isEmpty());
	}
	
	
}
