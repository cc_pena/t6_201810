package tests;

import org.junit.*;
import junit.framework.TestCase;
import model.data_structures.MyList;

public class MyListTest extends TestCase
{
	/**
	 * Lista que se probar�
	 */
	private MyList<Integer> lista;

	/**
	 * Prepara el escenario de pruebas
	 */
	public void setUp()
	{
		lista = new MyList<Integer>();
		lista.add(new Integer(10));
		lista.add(new Integer(-25));
		lista.add(new Integer(13));
		lista.add(new Integer(45));
		lista.add(new Integer(-1));
		lista.add(new Integer(-8));
		lista.add(new Integer(8));
	}

	public void testGet()
	{
		int a = lista.get(3);
		assertEquals("No es el numero esperado", 45, a);
		a = lista.get(6);
		assertEquals("No es el numero esperado", 8, a);
		a = lista.get(new Integer(-8));
		assertEquals("No es el numero esperado", -8, a);
		a = lista.get(new Integer(-25));
		assertEquals("No es el numero esperado", -25, a);
	}

	public void testDelete(){
		MyList<Integer> temp = lista;
		int a = temp.get(1);
		boolean b = temp.delete(new Integer(-25));
		a = temp.get(1);
		assertTrue("No elimina correctamente", a == 13 && b == true);
		b = temp.delete(new Integer(0));
		assertTrue("No elimina correctamente", b == false);
	}

	public void testSize(){
		MyList<Integer> temp = lista;
		assertTrue("No es el tama�o correcto", temp.size() == 7);
		temp.delete(new Integer(-25));
		assertTrue("No es el tama�o correcto", temp.size() == 6);
		lista.add(new Integer(83));
		lista.add(new Integer(134));
		assertTrue("No es el tama�o correcto", temp.size() == 8);
	}
	
	public void testMethod()
	{
		lista.listing();
		while(true)
		{
			try 
			{
				int a =lista.getCurrent();
				System.out.println("entra = "+ a);
				lista.avanzar();
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("aca muere");
				break;
			}
		}
	}
	
	public void testAddInOrder(){
		MyList<Integer> temp = new MyList<Integer>();
		
		temp.addInOrder(new Integer(23));
		
		String concatenado0 = "";
		for(int i = 0; i < temp.size(); i++){
			concatenado0 += temp.get(i) + "\n";
		}
		System.out.println(concatenado0);
		
		temp.addInOrder(new Integer(5));
		
		String concatenado1 = "";
		for(int i = 0; i < temp.size(); i++){
			concatenado1 += temp.get(i) + "\n";
		}
		System.out.println(concatenado1);
		
		temp.addInOrder(new Integer(2));
		
		String concatenado2 = "";
		for(int i = 0; i < temp.size(); i++){
			concatenado2 += temp.get(i) + "\n";
		}
		System.out.println(concatenado2);
		
		temp.addInOrder(new Integer(9));
		
		String concatenado3 = "";
		for(int i = 0; i < temp.size(); i++){
			concatenado3 += temp.get(i) + "\n";
		}
		System.out.println(concatenado3);
		
		temp.addInOrder(new Integer(1));
		
		
		temp.addInOrder(new Integer(-25));
		temp.addInOrder(new Integer(14));
		temp.addInOrder(new Integer(12));
		temp.addInOrder(new Integer(1598));
		String concatenado = "";
		for(int i = 0; i < temp.size(); i++){
			concatenado += temp.get(i) + "\n";
		}
		System.out.println(concatenado);
		assertTrue("No es el tama�o correcto", temp.size() == 9);
	}
}
