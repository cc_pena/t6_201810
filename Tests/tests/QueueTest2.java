package tests;

import org.junit.*;
import junit.framework.TestCase;
import model.data_structures.PriorityQueue;


/**
 * Clase usada para verificar que la estructura Stack (pila) est� correctamente implementada.
 */
public class QueueTest2 extends TestCase
{

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	private PriorityQueue<String> priorityQueue;

	public void setUp()
	{
		priorityQueue = new PriorityQueue<String>(80); 
		for (int i = 0; i < 70; i++) 
		{
			if( i < 10 )
			priorityQueue.insert("H0"+i);
			else
				priorityQueue.insert("H"+i);
		}
	}

	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------

	public void test()
	{
		setUp();
		System.out.println(priorityQueue.size());
		for (int i = 0; i < priorityQueue.size()+1; i++) 
		{
			System.out.println(priorityQueue.get(i));
			if(priorityQueue.get(i) != null && priorityQueue.get(i).equals("H21"))
			{
				System.out.println("ES ESTEEesghuiseyhse9hsuiehsejhseEE");
			}
			System.out.println("es " + i);
		}
		assertEquals("H69", priorityQueue.remove());
		assertEquals("H21", priorityQueue.remove());

	}


}
