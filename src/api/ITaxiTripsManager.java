package api;


import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.MyList;
import model.data_structures.Queue;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * API para la clase de logica principal  
 */
public interface ITaxiTripsManager 
{
	//1C
	/**
	 * Dada la direcci�n del json que se desea cargar, se generan VO's, estructuras y datos necesarias
	 * @param direccionJson, ubicaci�n del json a cargar
	 * @return true si se lo logr� cargar, false de lo contrario
	 */
	public boolean cargarSistema(String direccionJson, RangoFechaHora rango);
	
	/**
	 * Dada un �rea de comunidad
	 * por cada servicio debe informarse su Id y su tiempo de llegada a la parada. 
	 * Usar la implementaci�n de una Tabla de Hash (�rea, {Servicios}) con Separate Chaining. 
	 * @param llave int area
	 * @return servicios ordenados cronologicamente 
	 */
	public Queue<Servicio> serviciosDelArea(int pArea);
	
	/**
	 * Agrupar los servicios por la distancia recorrida en millas. Para tal fin, los grupos se conformar�n de la siguiente manera: 
	 * En el grupo 1 se agregar�n los servicios cuya distancia recorrida este entre 0 y 1 milla, el grupo 2 de 2.1 a 3 millas, 
	 * el grupo 3 de 21 a 30 millas, y as� sucesivamente. Definir una Tabla de Hash para representar los grupos con (Distancia, {Servicios}) 
	 * donde la Distancia es la distancia en millas recorrida en el servicio y {Servicios} es el conjunto de servicios asociado con esa distancia.
	 * Un grupo es v�lido si su conjunto de servicios No es vac�o. Obtener el conjunto {Servicios} a partir de una distancia de consulta usando 
	 * la tabla de hash. 
	 * 	
	 */
	public MyList<Servicio> serviciosDistancia(double pDistancia);
	
	

}
