package model.vo;

import model.data_structures.MyList;
import model.data_structures.PriorityQueue;

public class Chicago {
	//Atributos
	public PriorityQueue<Compania> city;
	public MyList<Servicio> servicios;

	//Constructor
	public Chicago()
	{
		city = new PriorityQueue(10000);
		servicios = new MyList<Servicio>();
	}

	//Metodos
	public PriorityQueue<Compania> darCompanias(){
		return city;
	}

	public MyList<Servicio> darServicios(){
		return servicios;
	}

	/**
	 * Busca la compania con el nombre dado por parametro
	 * @param company Nombre de la compania que se quiere
	 * @return compania con el nombre dado por parametro
	 */
	public Compania buscarCompania(String company)
	{
		Compania buscada = null;
		for(int i=0; i<10000; i++)
		{
			Compania actual = (Compania) city.get(i);
			if(actual != null && actual.nombre.equals(company))
			{
				buscada = actual;
				break;
			}
		}
		return buscada;
	}

//	/**
//	 * Busca un taxi segun su id
//	 * @param taxiId identificador asociado al taxi que se busca
//	 * @return Taxi con el id dado por parametro
//	 * 			null si no encuentra el taxi
//	 */
//	public Taxi buscarTaxi(String taxiId)
//	{
//		Taxi buscado = null;
//		city.listing();
//		while(city.getCurrent()!=null)
//		{	
//			buscado = city.getCurrent().buscarTaxi(taxiId);
//			if(buscado!=null)
//				break;
//			city.avanzar();
//			try
//			{
//				city.getCurrent();
//			} 
//			catch (Exception e) 
//			{
//				break;
//			}
//		}
//		return buscado;	
//	}

	/**
	 * Lista de servicios prestados en un rango dado por parametro
	 * @param rango Fecha y hora de inicio y fin en la que se encuentran los servicios
	 * @return Cola con los servicios prestados en el rango dado
	 */
	public Servicio[] serviciosEnRango(RangoFechaHora rango)
	{
		MyList<Servicio> enRango= new MyList<Servicio>();

		String inicio= rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String fin= rango.getFechaFinal()+"T"+rango.getHoraFinal();

		Servicio compararRango = new Servicio(inicio, fin);

		boolean empezo = false;
		servicios.listing();
		while(true)
		{
			try {
				servicios.getCurrent();
			} catch (Exception e) {
				break;
				// TODO: handle exception
			}
			Servicio actual=servicios.getCurrent();
			if(empezo)
			{
				//comparar fecha inicial del servicio con fecha final del rango
				if(!actual.getStartTime().before(compararRango.getEndTime()))
					break;
			}
			int temp=actual.compareTo(compararRango);
			if(temp == -1 || temp == 0 )
			{
				empezo=true;
				if(actual.getEndTime().before(compararRango.getEndTime()))
					enRango.add(servicios.getCurrent());
			}
			servicios.avanzar();
		}
		int x=enRango.size();
		
		Servicio[] respuesta = new Servicio[x];
		
		return respuesta;
	}
}