package model.vo;


import model.data_structures.MyList;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private MyList<Servicio> servicios;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public MyList<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(MyList<Servicio> servicios) {
		this.servicios = servicios;
	}

	
	/**
	 * Compara las companiaServicios segun la cantidad de servicios que tienen
	 * @return 0 si la cantidad de servicios de ambas companias es la misma
	 * 			1 si this tiene mas servicios que companiaServicio o
	 * 		   -1 si this tiene menos servicios que companiaServicio o
	 */
	@Override
	public int compareTo(CompaniaServicios o) {
		if(this.servicios.size() > o.servicios.size())
			return -1;
		else if(this.servicios.size() < o.servicios.size())
			return 1;
		else
			return 0;
		
	}
	
	

}
