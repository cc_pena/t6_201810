package model.vo;

import model.data_structures.MyList;
import model.data_structures.Queue;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	//----------------------
	//Atributos
	//------------------------

	/**Identificador del taxi*/
	private String id;

	/**La suma del dinero recibido por los servicios prestados */
	private double valorTotal;

	/**La distancia total recorrida en todos los servicios prestados */
	private double totalRecorrido;
	
	private int cantidadServicios;

	//Constructor
	public Taxi (String pID)
	{
		id = pID;
		valorTotal = 0;
		totalRecorrido = 0;
		cantidadServicios = 0;
	}
	
	public String getId(){
		return id;
	}
	
	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public double getTotalRecorrido() {
		return totalRecorrido;
	}

	public void setTotalRecorrido(double totalRecorrido) {
		this.totalRecorrido = totalRecorrido;
	}

	public int getCantidadServicios() {
		return cantidadServicios;
	}

	public void setCantidadServicios(int cantidadServicios) {
		this.cantidadServicios = cantidadServicios;
	}

	@Override
	public int compareTo(Taxi arg0) {
		// TODO Auto-generated method stub
		return this.getId()
				.compareTo(arg0.getId());
	}
}
