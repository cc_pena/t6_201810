package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.MyList;

public class ZonaServicios implements Comparable<ZonaServicios>
{

	private String idZona;
	
	private LinkedList<Servicio> fechasServicios;
	
	public ZonaServicios(String pidZona)
	{
		idZona = pidZona;
		fechasServicios = new MyList<Servicio>();
	}
	
	public String getIdZona() {
		return idZona;
	}



	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public LinkedList<Servicio> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(LinkedList<Servicio> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) {
		// TODO Auto-generated method stub
		Integer primero = Integer.parseInt(idZona);
		Integer segundo = Integer.parseInt(o.idZona);
		if(primero.compareTo(segundo) > 0)
			return -1;
		else if(primero.compareTo(segundo) < 0)
			return 1;
		return 0;
	}
}
