package model.vo;

import model.data_structures.MyList;

public class Compania implements Comparable<Compania> {
	
	public String nombre;
	
	public MyList<Taxi> taxiList;
	
	public MyList<Servicio> servicioList;
	
	
	public Compania(String pNombre){
		nombre = pNombre;
		taxiList = new MyList<Taxi>();
		servicioList = new MyList<Servicio>();
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MyList<Taxi> getTaxisInscritos() {
		return taxiList;
	}

	public void setTaxisInscritos(MyList<Taxi> taxisInscritos) {
		this.taxiList = taxisInscritos;
	}
	
	public MyList<Taxi> getMisTaxis(){
		return taxiList;
	}
	
	/**
	 * Busca un taxi en la compania segun el id
	 * @param id El id del taxi que se busca
	 * @return El taxi de la compania con el id dado por parametro
	 */
	public Taxi buscarTaxi(String id)
	{
		Taxi buscado = null;
		taxiList.listing();
		while(true)
		{
			if(taxiList.getCurrent().getId().equals(id))
			{
				buscado = taxiList.getCurrent();
				break;
			}
			taxiList.avanzar();
			try 
			{
				taxiList.getCurrent();
			} 
			catch (Exception e) 
			{
				break;
			}
		}
		return buscado;
		
	}
	
	/**
	 * Lista de servicios prestados en un rango dado por parametro
	 * @param rango Fecha y hora de inicio y fin en la que se encuentran los servicios
	 * @return Cola con los servicios prestados en el rango dado
	 */
	public MyList<Servicio> serviciosEnRango(RangoFechaHora rango)
	{
		MyList<Servicio> enRango= new MyList<Servicio>();

		String inicio= rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String fin= rango.getFechaFinal()+"T"+rango.getHoraFinal();

		Servicio compararRango = new Servicio(inicio, fin);

		boolean empezo = false;
		servicioList.listing();
		while(true)
		{
			Servicio actual=servicioList.getCurrent();
			if(empezo)
			{
				//comparar fecha inicial del servicio con fecha final del rango
				if(!actual.getStartTime().before(compararRango.getEndTime()))
					break;
			}
			int temp=actual.compareTo(compararRango);
			if(temp == -1 || temp == 0 )
			{
				empezo=true;
				if(actual.getEndTime().before(compararRango.getEndTime()))
					enRango.add(servicioList.getCurrent());
			}
			servicioList.avanzar();
			try {
				servicioList.getCurrent();
			} catch (Exception e) {
				break;
				// TODO: handle exception
			}
		}
		return enRango;
	}

	/**
	 * Compara la compania con la recibida por parametro segun la cantidad de servicios 
	 * prestados en el rango de tiempo de consulta
	 * @return 1 si THIS tiene prioridad MAYOR que la compania por parametro
	 * 		   0 si this y la compania por parametro tienen la misma prioridad
	 * 		  -1 si THIS tiene prioridad MENOR que la compania por parametro
	 */
	@Override
	public int compareTo(Compania o) {
		int servThis=servicioList.size();
		int servOtro=servicioList.size();
		if(servThis > servOtro)
			return 1;
		else if(servThis < servOtro)
			return -1;
		return 0;
	}
	
	public String toString ()
	{
		return nombre;
	}
}
