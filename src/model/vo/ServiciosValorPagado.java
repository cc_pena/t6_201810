package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.MyList;

public class ServiciosValorPagado {
	
	private MyList<Servicio> serviciosAsociados;
	private double valorAcumulado;
	public ServiciosValorPagado()
	{
		serviciosAsociados = new MyList<Servicio>();
		valorAcumulado = 0;
	}
	public MyList<Servicio> getServiciosAsociados() 
	{
		return serviciosAsociados;
	}
	public void setServiciosAsociados(MyList<Servicio> serviciosAsociados) 
	{
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado()
	{
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) 
	{
		this.valorAcumulado = valorAcumulado;
	}
	
	

}
