package model.logic;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.HashLinearProbing;
import model.data_structures.Heap;
import model.data_structures.LinkedList;
import model.data_structures.MyList;
import model.data_structures.PriorityQueue;
import model.data_structures.Queue;
import model.data_structures.SeparateChaining;
import model.data_structures.Stack;
import model.vo.Chicago;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager
{
	//Constantes
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";

	//Atributos
	private PriorityQueue<Taxi> prioTaxi;
    private SeparateChaining<Integer, MyList<Servicio>> separateChain;
	
	private Servicio[] serv;

	//Metodos
	public boolean cargarSistema(String direccionJson, RangoFechaHora pRango) 
	{
		JsonParser parser = new JsonParser();

		File[] array = new File[1];
		if(direccionJson.equals(DIRECCION_LARGE_JSON))
			array = new File(direccionJson).listFiles();
		else
			array[0] = new File(direccionJson);
		prioTaxi= new PriorityQueue<Taxi>(10000);
		separateChain = new SeparateChaining<Integer, MyList<Servicio>>(100);
		
		try 
		{
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			for(int a = 0; a < array.length; a++)
			{
				JsonArray arr= (JsonArray) parser.parse(new FileReader(array[a]));
				/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
				for (int i = 0; arr != null && i < arr.size(); i++)
				{
					JsonObject obj= (JsonObject) arr.get(i);

					/* Usar el parametro rango para comparar con startimestamp y endtimestamp*/
					String startTime = obj.get("trip_start_timestamp") == null? "NaN": obj.get("trip_start_timestamp").getAsString();
					String endTime = obj.get("trip_end_timestamp") == null? "NaN": obj.get("trip_end_timestamp").getAsString();

					Date start = null;

					try {
						start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(startTime);
					} catch (ParseException e) {
						//Exception si el string no pudo pasarse al formato de la fecha
						e.printStackTrace();
					}

					Date end = null;
					try {
						end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(endTime);
					} catch (ParseException e) {
						//Exception si el string no pudo pasarse al formato de la fecha
						e.printStackTrace();
					}

					/* Obtener la compania del taxi*/
					/* Verificar que el servicio este en el rango*/
					if(!start.before(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(pRango.getFechaInicial() + "T" + pRango.getHoraInicio())) && !end.after(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(pRango.getFechaFinal() + "T" + pRango.getHoraFinal())))
					{	
						System.out.println("entra aca --- " + i);

						/* Obtener la propiedad taxi_id de un taxi (String)*/
						String taxi_id = obj.get("taxi_id") == null? "NaN": obj.get("taxi_id").getAsString();
						Taxi taxi = new Taxi(taxi_id);

						boolean entra = false;
						for(int j=0; j<prioTaxi.size(); j++)
						{
							Taxi actual = (Taxi) prioTaxi.get(j);
							if(actual != null && actual.getId().equals(taxi_id))
							{
								taxi = actual;
								entra = true;
								break;
							}
						}

						double trip_miles = obj.get("trip_miles") == null? 0: obj.get("trip_miles").getAsDouble();
						double trip_total = obj.get("trip_total") == null? 0: obj.get("trip_total").getAsDouble();
						taxi.setTotalRecorrido(taxi.getTotalRecorrido() + trip_miles);
						taxi.setValorTotal(taxi.getValorTotal() + trip_total);
						taxi.setTotalRecorrido(taxi.getTotalRecorrido() + 1);
						if(entra == false)
						{
							prioTaxi.insert(taxi);
						}

						String trip_id = obj.get("trip_id") == null? "NaN": obj.get("trip_id").getAsString();
						double dropoff_census_tract = obj.get("dropoff_census_tract") == null? 0: obj.get("dropoff_census_tract").getAsDouble();
						double dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude") == null? 0: obj.get("dropoff_centroid_latitude").getAsDouble();

						JsonElement drop = obj.get("dropoff_centroid_location");
						String dropoff_type = drop == null? "NaN": drop.getAsJsonObject().get("type").getAsString();
						double droplat = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
						double droplong = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();

						double dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude") == null? 0: obj.get("dropoff_centroid_longitude").getAsDouble();
						int dropoff_community_area = obj.get("dropoff_community_area") == null? 0: obj.get("dropoff_community_area").getAsInt();

						double extras = obj.get("extras") == null? 0: obj.get("extras").getAsDouble();
						double fare = obj.get("fare") == null? 0: obj.get("fare").getAsDouble();
						String payment_type = obj.get("payment_type") == null? "NaN": obj.get("payment_type").getAsString();

						double pickup_census_tract = obj.get("pickup_census_tract") == null? 0: obj.get("pickup_census_tract").getAsDouble();
						double pickup_centroid_latitude = obj.get("pickup_centroid_latitude") == null? 0: obj.get("pickup_centroid_latitude").getAsDouble();

						JsonElement pick = obj.get("pickup_centroid_location");
						String pickup_type = pick == null? "NaN": pick.getAsJsonObject().get("type").getAsString();
						double picklat = pick == null? 0: pick.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
						double picklong = pick == null? 0: pick.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();

						double pickup_centroid_longitude = obj.get("pickup_centroid_longitude") == null? 0: obj.get("pickup_centroid_longitude").getAsDouble();
						int pickup_community_area = obj.get("pickup_community_area") == null? 0: obj.get("pickup_community_area").getAsInt();

						String taxiAutor = taxi_id;
						double tips = obj.get("tips") == null? 0: obj.get("tips").getAsDouble();
						double tolls = obj.get("tolls") == null? 0: obj.get("tolls").getAsDouble();

						String trip_end_timestamp = obj.get("trip_end_timestamp") == null? "NaN": obj.get("trip_end_timestamp").getAsString();
						int trip_seconds = obj.get("trip_seconds") == null? 0: obj.get("trip_seconds").getAsInt();
						String trip_start_timestamp = obj.get("trip_start_timestamp") == null? "NaN": obj.get("trip_start_timestamp").getAsString();

						Servicio serv = new Servicio(trip_id, dropoff_census_tract, dropoff_centroid_latitude, dropoff_type, droplat, droplong, dropoff_centroid_longitude, dropoff_community_area, extras, fare, payment_type, pickup_census_tract, pickup_centroid_latitude, pickup_type, picklat, picklong, pickup_centroid_longitude, pickup_community_area, taxiAutor, tips, tolls, trip_end_timestamp, trip_miles, trip_seconds, trip_start_timestamp, trip_total);

						MyList<Servicio> servList = separateChain.get(new Integer(pickup_community_area));
						if(servList == null)
						{
							servList = new MyList<Servicio>();
							separateChain.put(new Integer(pickup_community_area), servList);
						}
						servList.addInOrder(serv);
					}
				}
			}
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Dada un �rea de comunidad
	 * por cada servicio debe informarse su Id y su tiempo de llegada a la parada. 
	 * Usar la implementaci�n de una Tabla de Hash (�rea, {Servicios}) con Separate Chaining. 

	 * @param llave int area
	 * @return servicios ordenados cronologicamente 
	 */
	public Queue<Servicio> serviciosDelArea(int pArea)
	{
		Queue<Servicio> respuesta = new Queue<Servicio>();
		MyList<Servicio> servList = separateChain.get(new Integer(pArea));
		if(servList != null)
		{
			servList.listing();
			try 
			{
				servList.getCurrent();
				while(true)
				{
					respuesta.enqueue(servList.getCurrent());
					servList.avanzar();
					try 
					{
						servList.getCurrent();
					} 
					catch (Exception e)
					{
						break;
					}
				}
			} 
			catch (Exception e)
			{
				return respuesta;
			}	
		}
		return respuesta;
	}


	/**
	 * Agrupar los servicios por la distancia recorrida en millas.
	 * En el grupo 1 se agregar�n los servicios cuya distancia recorrida este entre 0 y 1 milla, el grupo 2 de 2.1 a 3 millas y as� sucesivamente. Definir una Tabla de Hash para representar los grupos con (Distancia, {Servicios}) 
	 * Distancia es la distancia en millas recorrida en el servicio 
	 * {Servicios} es el conjunto de servicios asociado con esa distancia.
	 * No hay grupos sin servicios
	 * @return Conjunto {Servicios} a partir de una distancia de consulta usando la tabla de hash. 
	 */
	@Override
	public MyList<Servicio> serviciosDistancia(double pDistancia) {	
		HashLinearProbing<RangoDistancia, MyList<Servicio>> agrupados = new HashLinearProbing<RangoDistancia, MyList<Servicio>>(11);
		RangoDistancia keyRespuesta = new RangoDistancia();
		keyRespuesta.setLimineInferior(pDistancia);keyRespuesta.setLimiteSuperior(pDistancia+1);
		
		for(int i=0; i < serv.length; i++)
		{
			Servicio actual = serv[i];
			int inf =(int) actual.getTripMiles();
			RangoDistancia esteRegistro = new RangoDistancia(); esteRegistro.setLimineInferior(inf);esteRegistro.setLimiteSuperior(inf+1);
			MyList<Servicio> deKey = agrupados.get(esteRegistro);
			if(deKey != null)
			{
				deKey.addInOrder(actual);
			}
			else
			{
				RangoDistancia nuevoRango = new RangoDistancia(); 	
				MyList<Servicio> servDeRango = new MyList<Servicio>();
				nuevoRango.setLimineInferior(inf);
				nuevoRango.setLimiteSuperior(inf + 1);
				servDeRango.add(actual);
				agrupados.put(nuevoRango, servDeRango);
			}
		}
		MyList<Servicio> respuesta = agrupados.get(keyRespuesta);
		return respuesta;
	}
}
