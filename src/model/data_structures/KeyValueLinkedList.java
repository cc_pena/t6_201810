package model.data_structures;

import java.util.ArrayList;

public class KeyValueLinkedList<K, V> {
    private int size;
    private HashNode<K,V> first; 
    
    public KeyValueLinkedList() 
    {
		first = null;
    }

    public int size() 
    {
        return size;
    }

    
    public boolean isEmpty() 
    {
        return size == 0;
    }

    public boolean contains(K key) 
    {
        if (key == null)
        {
        	System.out.println(this.getClass() + ": la llave es invalida");
        	return false;
        }
        return get(key) != null;
    }

    public V get(K key) 
    {
    	if (key == null)
        {
        	System.out.println(this.getClass() + ": la llave es invalida");
        	return null;
        }
    	
        HashNode<K,V> x = first;
        while(x != null) 
        {
            if (key.equals(x.getKey()))
            {
            	return x.getValue();
            }
            x = x.next;
        }
        return null;
    }

    public void put(K key, V value) 
    {
    	if (key == null)
        {
        	System.out.println(this.getClass() + ": la llave es invalida");
        	return;
        }
    	
        if (value == null) 
        {
            delete(key);
            return;
        }

        HashNode<K,V> x = first;
        boolean entro = false;
        while(x != null) 
        {
            if (key.equals(x.getKey()))
            {
            	x.value = value;
            	entro = true;
            	return;
            }
            x = x.next;
        }
        HashNode<K,V> temp = new HashNode<K,V>(key, value);
        temp.next = first;
        first = temp;
        if(entro == false)
        {
        	size++;
        }
    }

    public void delete(K key) 
    {
    	if (key == null)
        {
        	System.out.println(this.getClass() + ": la llave es invalida");
        	return;
        }
    	HashNode<K,V> x = first;
    	HashNode<K,V> before = null;
        while(x != null) 
        {
            if (key.equals(x.getKey()))
            {
            	size--;
            	if(before == null)
            	{
            		first = first.next;
            	}
            	else
            	{
            		before.next = x.next;
            	}
            }
            before = x;
            x = x.next;
        }
    }
    
    public Iterable<K> keys()  
    {
        ArrayList<K> queue = new ArrayList<K>();
        for (HashNode<K,V> x = first; x != null; x = x.next)
            queue.add(x.key);
        return queue;
    }
}