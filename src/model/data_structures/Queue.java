package model.data_structures;

import model.vo.RangoDistancia;

public class Queue <T> implements IQueue<T>
{
	//--------------------------
	//Atributos
	//--------------------------
	
	/**
	 * The amount of elements in the queue
	 */
	protected int size;
	
	/**
	 * The first node in the queue
	 */
	protected Node<T> first;
	
	/**
	 * The last node in the queue
	 */
	protected Node<T> lastNode;
	
	/**
	 * The node which information we can see now 
	 */
	protected Node<T> actualNode;
	
	//-------------------
	//Constructor
	//-------------------
	public Queue(){
		first = null;
		lastNode = null;
		actualNode = null;
	}
	
	//-------------------
	//M�todos
	//-------------------
	@Override
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		if(isEmpty()){
			first = new Node<T>(item);
			actualNode = first;
			lastNode = first;
		}
		else{
			actualNode = lastNode;
			lastNode = new Node<T>(item);
			actualNode.next = lastNode;
			actualNode = first;
		}
		size++;
	}
	
	/** Enqueue a new element at the end of the queue */
	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		if(isEmpty())
			return null;
		T item = first.getElement();
		first = first.next;
		if(first == null){
			lastNode = null;
			actualNode = null;
		}
		size--;
		return item;
	}
	
	public void listing(){
		actualNode = first;
	}
	
	/** Evaluate if the queue is empty. 
	 * @return true if the queue is empty. false in other case.
	 */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null? true:false;
	}
	
	/**
	 * Calculate the amount of elements in the queue
	 * @return number of elements in the queue
	 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}


}