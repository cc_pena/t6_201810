package model.data_structures;

public class HashLinearProbing<K, V> {

	public final static int LIMITE = 5;
	public int size;
	public int limiteActual;
	public K[] keys;
	public V[] values;

	/**
	 * Construye una tabla de hash lineal
	 * @param pCapacidad El limite inicial
	 */
	public HashLinearProbing(int pCapacidad) 
	{
		limiteActual = pCapacidad;
		keys = (K[]) new Object[limiteActual];
		values = (V[]) new Object[limiteActual];
		size = 0;
	}

	public int size() 
	{
		return size;
	}

	public boolean contains(K key)
	{
		return get(key) != null;
	}

	public int hash(K key) 
	{
		return (key.hashCode() & 0x7fffffff) % limiteActual;
	}

	private void resize(int capacity) 
	{
		HashLinearProbing<K, V> temp = new HashLinearProbing<K, V>(capacity*2);
		for (int i = 0; i < limiteActual; i++) 
		{
			if (keys[i] != null) 
			{
				temp.put(keys[i], values[i]);
			}
		}
		keys = temp.keys;
		values = temp.values;
		limiteActual = temp.limiteActual;
	}

	public void put(K key, V value) 
	{
		if (value == null) 
		{
			delete(key);
			return;
		}

		//Se ejecuta resize si el limite supera el 0.8
		if (size >= limiteActual*0.8) resize(limiteActual);

		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % limiteActual) 
		{
			if (keys[i].equals(key)) {
				values[i] = value;
				return;
			}
		}
		keys[i] = key;
		values[i] = value;
		size++;
	}

	public V get(K key) 
	{
		if (key == null) 
		{
			System.out.println(this.getClass() + "En el metodo get: la key no es valida");
			return null;
		}
		for (int i = hash(key); keys[i] != null; i = (i + 1) % limiteActual){
			if (keys[i].equals(key))
				return values[i];
		}
		return null;
	}

	public V delete(K key) {
		if (key == null) 
		{
			System.out.println(this.getClass() + "En el metodo delete: la key no es valida");
			return null;
		}
		if (!contains(key)) return null;

		int i = hash(key);
		while (!key.equals(keys[i])) 
		{
			i = (i + 1) % limiteActual;
		}

		keys[i] = null;
		V vreturn = values[i];
		values[i] = null;

		i = (i + 1) % limiteActual;
		while (keys[i] != null) 
		{
			// delete keys[i] an vals[i] and reinsert
			K   keyToRehash = keys[i];
			V valToRehash = values[i];
			keys[i] = null;
			values[i] = null;
			size--;
			put(keyToRehash, valToRehash);
			i = (i + 1) % limiteActual;
		}

		size--;

		//Dividir la tabla en la mitad si esta menos de la mitad llena
		if (size > 0 && size < limiteActual/2)
		{
			// es dividido 4 ya que el resize hace *2 por dentro
			resize(limiteActual/4);
		}
		return vreturn;
	}

	public Queue<K> keys() 
	{
		Queue<K> queue = new Queue<K>();
		for (int i = 0; i < limiteActual; i++)
			if (keys[i] != null) queue.enqueue(keys[i]);
		return queue;
	}


	public static void main(String[] args) 
	{ 
		HashLinearProbing<String, Integer> st = new HashLinearProbing<String, Integer>(10);

		String StdIn = "aaabccdddefgh";
        for (int i = 0; i < StdIn.length(); i++) {
            String key = ""+ StdIn.charAt(i);
            st.put(key, i);
        }
        Queue<String> queue = st.keys(); 
		while(!queue.isEmpty())
		{
			String s = queue.dequeue();
			System.out.println(s + " " + st.get(s)); 
		}
        System.out.println("el tama�o es " + st.size + "\n");
        System.out.println("aca empieza el remove");
        st.delete("d");
        queue = st.keys(); 
		while(!queue.isEmpty())
		{
			String s = queue.dequeue();
			System.out.println(s + " " + st.get(s)); 
		}
		System.out.println("el tama�o es " + st.size + "\n");
		
	}
}