package model.data_structures;

public class Stack <T extends Comparable<T>> implements IStack<T>
{
	//----------------------------
	//Atributos
	//----------------------------
	/** The amount of elements in the stack */
	public int size;
	
	/** The last element in, the first reference */
	protected Node<T> top;
	
	/** The current node */
	protected Node<T> actualNode;
	
	//Constructor
	public Stack(){
		top = null;
		actualNode = top;
	}
	
	//---------------------------
	//M�todos
	//---------------------------
	
	/** Push a new element at the top of the stack */
	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		if(isEmpty()){
			top = new Node<T>(item);
			actualNode = top;
		}
		else{
			actualNode = top;
			top = new Node<T>(item);
			top.next = actualNode;
		}
		size++;
	}

	/** Pop the element at the top of the stack 
	 * Take off the top element
	 * @return the top element or null if it doesn't exist
	 */
	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if(isEmpty())
			return null;
		T item = top.getElement();
		top = top.getNext();
		size--;
		return item;
	}
	
	
	public boolean get(T element) {
		listing();
		while(actualNode != null){
			if(actualNode.getElement().compareTo(element)==0){
				return true;
			}
			actualNode = actualNode.getNext();
		}
		return false;
	}
	
	public void listing(){
		actualNode = top;
	}
	
	/** Evaluate if the stack is empty
	 * @return true if the stack is empty. false in other case. 
	 */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return top == null? true:false;
	}

}
