package model.data_structures;
import java.util.ArrayList;

public class SeparateChaining<K, V> 
{
	public int size; 
	public int limiteActual;  
	public KeyValueLinkedList<K, V>[] st;

	@SuppressWarnings("unchecked")
	public SeparateChaining(int limite) 
	{
		this.limiteActual = limite;
		st = (KeyValueLinkedList<K, V> []) new KeyValueLinkedList[limite];
		for (int i = 0; i < limiteActual; i++)
		{
			st[i] = new KeyValueLinkedList<K, V>();
		}
	} 

	private void resize(int capacity) 
	{
		SeparateChaining<K, V> temp = new SeparateChaining<K, V>(capacity*2);
		for (int i = 0; i < limiteActual; i++) 
		{
			for (K key : st[i].keys()) 
			{
				temp.put(key, st[i].get(key));
			}
		}
		this.limiteActual  = temp.limiteActual;
		this.size  = temp.size;
		this.st = temp.st;
	}

	public int hash(K key) 
	{
		return (key.hashCode() & 0x7fffffff) % limiteActual;
	} 

	public int size() 
	{
		return size;
	} 

	public boolean isEmpty() 
	{
		return size == 0;
	}

	public boolean contains(K key) 
	{
		if (key == null)
		{
			System.out.println(this.getClass() + " en el metodo contains: la llave es invalida");
			return false;
		}
		return get(key) != null;
	} 

	public V get(K key) 
	{
		if (key == null)
		{
			System.out.println(this.getClass() + " en el metodo get: la llave es invalida");
			return null;
		}
		int i = hash(key);
		return st[i].get(key);
	} 

	public void put(K key, V val) 
	{
		if (key == null)
		{
			System.out.println(this.getClass() + ": la llave es invalida");
			return;
		}
		if (val == null) 
		{
			delete(key);
			return;
		}
		//S ejecuta resize si el limite supera el 80% del limite
		if (size >= limiteActual*0.8)
		{
			resize(limiteActual);
		}

		int i = hash(key);
		if(st[i].contains(key) == false)
		{
			size++;
		}
		st[i].put(key, val);
	} 

	public V delete(K key) 
	{
		if (key == null)
		{
			System.out.println(this.getClass() + " en el metodo delete: la llave es invalida");
			return null;
		}

		int i = hash(key);
		V valor = st[i].get(key);
		if(st[i].contains(key) == true)
		{
			size--;
		}
		st[i].delete(key);

		//Dividir la tabla en la mitad si esta menos de la mitad llena
		if (size > 0 && size < limiteActual/2)
		{
			// es dividido 4 ya que el resize hace *2 por dentro
			resize(limiteActual/4);
		}
		return valor;
	} 

	// return keys in symbol table as an Iterable
	public Queue<K> keys() 
	{
		Queue<K> queue = new Queue<K>();
		for (int i = 0; i < limiteActual; i++) 
		{
			for (K key : st[i].keys())
				queue.enqueue(key);
		}
		return queue;
	} 
}
