package model.data_structures;

import model.vo.Compania;

public class PriorityQueue<T> 
{
	//Atributos
	/** Arreglo que*/
	private Comparable<T>[] heap;
	private int capacidad;
	private int size;

	// Constructor 
	public PriorityQueue(int pCapacidad)
	{    
		this.capacidad = pCapacidad + 1;
		heap = new Comparable[capacidad] ;
		size = 0;
	}

	/** Vacia el heap **/
	public void clear()
	{
		heap = new Comparable[capacidad];
		size = 0;
	}

	/** Indica si esta vacio = true, false si tiene al menos un elemento **/
	public boolean isEmpty()
	{
		return size == 0;
	}

	/** Indica si todas las posiciones del arreglo est�n ocupadas = true, false si hay al menos una libre **/
	public boolean isFull()
	{
		return size == capacidad - 1;
	}
	
	public Comparable<T>[] getHeapArray()
	{
		return heap;
	}
	
	/** Retorna el tama�o del arreglo **/
	public int size()
	{
		return size;
	}

	/** Inserta un elemento segun su prioridad
	 * Si la prioridad del nuevo elemento es mayor a la prioridad del elemento en la 
	 * posicion n/2 se va subiendo el elemento nuevo hasta que su prioridad sea menor que los que 
	 * estan antes que el
	 * @return 
	 **/
	public Comparable<T> insert(Comparable<T> pNuevo)
	{
		Comparable<T> nuevo = pNuevo;

		heap[++size] = nuevo;
		int pos = size;
		while (pos != 1)
		{
			if(nuevo.compareTo((T) heap[pos/2])>0)
			{
				heap[pos] = heap[pos/2];
				pos /=2;
			}
			else
				break;
		}
		heap[pos] = nuevo;  
		return nuevo;
	}
	/** Elimina el elemento de la cola con mayor prioridad manteniendo el orden de prioridad en el resto de la cola
	 * Retorna el elemento eliminado, null si el heap esta vacio
	 * Para reordenar al final de remover el elemento lo baja (lo pone mas cerca del final del arreglo) si
	 * su prioridad es menor que la del que sigue en el arreglo (CompareTo<0 **/
	public Comparable<T> remove()
	{
		if (isEmpty() )
		{
			return null;
		}
		Comparable<T> eliminar = heap[1];
		Comparable<T> temp = heap[size--];
		int padre = 1;
		int hijo = 2;
		while (hijo <= size)
		{
			if (hijo < size)
			{	 
				if(heap[hijo].compareTo((T) heap[hijo + 1])<0)
					hijo++;
			}
			if (heap[hijo].compareTo((T) heap[hijo + 1])>=0)
				break;

			heap[padre] = heap[hijo];
			padre = hijo;
			hijo *= 2;
		}
		heap[padre] = temp;
		return eliminar;
	}
	
	public Comparable<T> get(int position)
	{
		return (Comparable<T>) heap[position];
	}

}
