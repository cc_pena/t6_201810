package controller;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.MyList;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	public static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson, RangoFechaHora rango)
	{
		return manager.cargarSistema(direccionJson, rango);
	}
	
	public static Queue<Servicio> serviciosDelArea(int pArea)
	{
		return manager.serviciosDelArea(pArea);
	}
	
	public static MyList<Servicio> servicioDeDistancia(String pDistancia)
	{
		double dist = Double.parseDouble(pDistancia);
		return manager.serviciosDistancia(dist);
	}
	
}
